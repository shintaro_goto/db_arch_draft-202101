--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-2.pgdg90+1)
-- Dumped by pg_dump version 10.5

-- Started on 2021-01-04 11:28:55 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16438)
-- Name: ae_extract_dict; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.ae_extract_dict (
    ae_extract_id bigint NOT NULL,
    ae_extract_name character varying(128)
);


ALTER TABLE public.ae_extract_dict OWNER TO root;

--
-- TOC entry 214 (class 1259 OID 16562)
-- Name: ae_extract_dict_ae_extract_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.ae_extract_dict_ae_extract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ae_extract_dict_ae_extract_id_seq OWNER TO root;

--
-- TOC entry 200 (class 1259 OID 16445)
-- Name: ae_std_dict; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.ae_std_dict (
    ae_std_dict_id bigint NOT NULL,
    ae_extract_id bigint,
    ae_extract_name_variation character varying(128)
);


ALTER TABLE public.ae_std_dict OWNER TO root;

--
-- TOC entry 215 (class 1259 OID 16571)
-- Name: ae_std_dict_ae_std_dict_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.ae_std_dict_ae_std_dict_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ae_std_dict_ae_std_dict_id_seq OWNER TO root;

--
-- TOC entry 213 (class 1259 OID 16552)
-- Name: allergy; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.allergy (
    yj_code character(12),
    allergy_master_id bigint
);


ALTER TABLE public.allergy OWNER TO root;

--
-- TOC entry 212 (class 1259 OID 16545)
-- Name: allergy_master; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.allergy_master (
    allergy_master_id bigint NOT NULL,
    allergy_name character varying(128)
);


ALTER TABLE public.allergy_master OWNER TO root;

--
-- TOC entry 216 (class 1259 OID 16573)
-- Name: allergy_master_allergy_master_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.allergy_master_allergy_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.allergy_master_allergy_master_id_seq OWNER TO root;

--
-- TOC entry 217 (class 1259 OID 16575)
-- Name: contraindication; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.contraindication (
    contraindication_id bigint NOT NULL,
    yj_code character(12),
    subject character varying(128),
    patient_property integer,
    warning_level character varying(64),
    description text
);


ALTER TABLE public.contraindication OWNER TO root;

--
-- TOC entry 218 (class 1259 OID 16583)
-- Name: contraindication_contraindication_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.contraindication_contraindication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contraindication_contraindication_id_seq OWNER TO root;

--
-- TOC entry 219 (class 1259 OID 16585)
-- Name: day_limitation; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.day_limitation (
    day_limitation_id bigint NOT NULL,
    dps_code character(12),
    limitation_category character varying(64),
    limitation_days integer,
    expiration_date date
);


ALTER TABLE public.day_limitation OWNER TO root;

--
-- TOC entry 220 (class 1259 OID 16592)
-- Name: day_limitation_day_limitation_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.day_limitation_day_limitation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.day_limitation_day_limitation_id_seq OWNER TO root;

--
-- TOC entry 207 (class 1259 OID 16510)
-- Name: dose; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.dose (
    dose_id bigint NOT NULL,
    yj_code character(12),
    route character varying(64),
    treatment character varying(64),
    dose text
);


ALTER TABLE public.dose OWNER TO root;

--
-- TOC entry 221 (class 1259 OID 16594)
-- Name: dose_dose_id; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.dose_dose_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dose_dose_id OWNER TO root;

--
-- TOC entry 235 (class 1259 OID 16696)
-- Name: drug_basic_info; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.drug_basic_info (
    drug_basic_info_id bigint NOT NULL,
    updated_date date,
    yj_code character(12),
    dps_registration_date date,
    launch_date date,
    route character varying(64),
    strength character varying(128),
    maker_name text,
    generic_drug boolean,
    brand_drug boolean,
    brand_drug_available boolean,
    expiration_date date,
    shusai_date date,
    regulation character varying(128),
    ingredients integer,
    high_risk boolean,
    basic_drug boolean,
    alterable boolean
);


ALTER TABLE public.drug_basic_info OWNER TO root;

--
-- TOC entry 222 (class 1259 OID 16621)
-- Name: drug_basic_info_drug_basic_info_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.drug_basic_info_drug_basic_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drug_basic_info_drug_basic_info_id_seq OWNER TO root;

--
-- TOC entry 223 (class 1259 OID 16623)
-- Name: drug_identification; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.drug_identification (
    drug_identification_id bigint NOT NULL,
    modified date,
    yj_code character(12),
    imprint character varying(64),
    packaging_identifier character varying(64),
    color character varying(64),
    splittable boolean,
    form character varying(64),
    manufacturer text,
    producer text,
    seller text,
    appearance character varying(64),
    maker_mark character varying(64)
);


ALTER TABLE public.drug_identification OWNER TO root;

--
-- TOC entry 224 (class 1259 OID 16633)
-- Name: drug_identification_drug_identification_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.drug_identification_drug_identification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drug_identification_drug_identification_id_seq OWNER TO root;

--
-- TOC entry 225 (class 1259 OID 16635)
-- Name: drug_price; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.drug_price (
    drug_price_id bigint NOT NULL,
    modified date,
    dps_code character(12),
    price numeric(12,2)
);


ALTER TABLE public.drug_price OWNER TO root;

--
-- TOC entry 226 (class 1259 OID 16642)
-- Name: drug_price_drug_price_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.drug_price_drug_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drug_price_drug_price_id_seq OWNER TO root;

--
-- TOC entry 201 (class 1259 OID 16452)
-- Name: early_symptom; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.early_symptom (
    early_symptom_id bigint NOT NULL,
    ae_extract_id bigint,
    early_symptom_label character varying(128),
    early_symptom_content text
);


ALTER TABLE public.early_symptom OWNER TO root;

--
-- TOC entry 227 (class 1259 OID 16644)
-- Name: early_symptom_early_symptom_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.early_symptom_early_symptom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.early_symptom_early_symptom_id_seq OWNER TO root;

--
-- TOC entry 205 (class 1259 OID 16491)
-- Name: efficacy_class; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.efficacy_class (
    general_code character(12) NOT NULL,
    efficacy_class_id bigint NOT NULL
);


ALTER TABLE public.efficacy_class OWNER TO root;

--
-- TOC entry 202 (class 1259 OID 16469)
-- Name: efficacy_class_master; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.efficacy_class_master (
    efficacy_class_id bigint NOT NULL,
    efficacy_class_name character varying(128)
);


ALTER TABLE public.efficacy_class_master OWNER TO root;

--
-- TOC entry 228 (class 1259 OID 16650)
-- Name: efficacy_class_master_efficacy_class_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.efficacy_class_master_efficacy_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.efficacy_class_master_efficacy_class_id_seq OWNER TO root;

--
-- TOC entry 204 (class 1259 OID 16486)
-- Name: efficacy_class_relation; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.efficacy_class_relation (
    parent_efficacy_class_id bigint NOT NULL,
    child_efficacy_class_id bigint NOT NULL
);


ALTER TABLE public.efficacy_class_relation OWNER TO root;

--
-- TOC entry 203 (class 1259 OID 16476)
-- Name: efficacy_class_std_dict; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.efficacy_class_std_dict (
    efficacy_class_std_dict_id bigint NOT NULL,
    efficacy_class_id bigint,
    efficacy_class_name_variation character varying
);


ALTER TABLE public.efficacy_class_std_dict OWNER TO root;

--
-- TOC entry 229 (class 1259 OID 16654)
-- Name: efficacy_class_std_dict_efficacy_class_std_dict_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.efficacy_class_std_dict_efficacy_class_std_dict_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.efficacy_class_std_dict_efficacy_class_std_dict_id_seq OWNER TO root;

--
-- TOC entry 196 (class 1259 OID 16385)
-- Name: general_master; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.general_master (
    general_code character(12) NOT NULL,
    general_name character varying(128),
    general_name_ruby character varying(128),
    active_ingredients text
);


ALTER TABLE public.general_master OWNER TO root;

--
-- TOC entry 209 (class 1259 OID 16527)
-- Name: guidance; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.guidance (
    yj_code character(12) NOT NULL,
    guidance_id bigint NOT NULL
);


ALTER TABLE public.guidance OWNER TO root;

--
-- TOC entry 208 (class 1259 OID 16518)
-- Name: guidance_master; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.guidance_master (
    guidance_id bigint NOT NULL,
    guidance_content text,
    guidance_category character varying(64),
    guidance_category2 character varying(64)
);


ALTER TABLE public.guidance_master OWNER TO root;

--
-- TOC entry 230 (class 1259 OID 16664)
-- Name: guidance_master_guidance_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.guidance_master_guidance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guidance_master_guidance_id_seq OWNER TO root;

--
-- TOC entry 211 (class 1259 OID 16540)
-- Name: high_risk_guidance; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.high_risk_guidance (
    yj_code character(12) NOT NULL,
    high_risk_guidance_id bigint NOT NULL
);


ALTER TABLE public.high_risk_guidance OWNER TO root;

--
-- TOC entry 210 (class 1259 OID 16532)
-- Name: high_risk_guidance_master; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.high_risk_guidance_master (
    high_risk_guidance_id bigint NOT NULL,
    high_risk_guidance_content text,
    high_risk_guidance_category character varying(128),
    high_risk_guidance_category_aux character varying(128)
);


ALTER TABLE public.high_risk_guidance_master OWNER TO root;

--
-- TOC entry 231 (class 1259 OID 16670)
-- Name: high_risk_guidance_master_high_risk_guidance_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.high_risk_guidance_master_high_risk_guidance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.high_risk_guidance_master_high_risk_guidance_id_seq OWNER TO root;

--
-- TOC entry 206 (class 1259 OID 16502)
-- Name: indication_dose_text; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.indication_dose_text (
    indication_dose_text_id bigint NOT NULL,
    yj_code character(12),
    indication text,
    dosage text
);


ALTER TABLE public.indication_dose_text OWNER TO root;

--
-- TOC entry 232 (class 1259 OID 16672)
-- Name: indication_dose_text_indication_dose_text_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.indication_dose_text_indication_dose_text_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indication_dose_text_indication_dose_text_id_seq OWNER TO root;

--
-- TOC entry 233 (class 1259 OID 16674)
-- Name: interaction; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.interaction (
    interaction_id bigint NOT NULL,
    general_code_left character(9),
    general_code_right character(9),
    warning_level character varying(16),
    treatment text,
    interaction_factor text,
    CONSTRAINT interaction_general_code CHECK ((general_code_left < general_code_right))
);


ALTER TABLE public.interaction OWNER TO root;

--
-- TOC entry 234 (class 1259 OID 16683)
-- Name: interaction_interaction_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.interaction_interaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.interaction_interaction_id_seq OWNER TO root;

--
-- TOC entry 197 (class 1259 OID 16393)
-- Name: yj_codes; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.yj_codes (
    yj_code character(12) NOT NULL,
    dps_code character(12),
    individual_receipt_code character(9),
    generic_receipt_code character(9),
    general_code character(12),
    product_name character varying(128),
    product_name_ruby character varying(128)
);


ALTER TABLE public.yj_codes OWNER TO root;

--
-- TOC entry 198 (class 1259 OID 16398)
-- Name: yj_transition; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.yj_transition (
    old_yj_code character(12) NOT NULL,
    current_yj_code character(12) NOT NULL
);


ALTER TABLE public.yj_transition OWNER TO root;

--
-- TOC entry 2870 (class 2606 OID 16442)
-- Name: ae_extract_dict ae_extract_dict_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.ae_extract_dict
    ADD CONSTRAINT ae_extract_dict_pkey PRIMARY KEY (ae_extract_id);


--
-- TOC entry 2872 (class 2606 OID 16444)
-- Name: ae_extract_dict ae_extract_name; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.ae_extract_dict
    ADD CONSTRAINT ae_extract_name UNIQUE (ae_extract_name);


--
-- TOC entry 2874 (class 2606 OID 16570)
-- Name: ae_std_dict ae_std_dict_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.ae_std_dict
    ADD CONSTRAINT ae_std_dict_pkey PRIMARY KEY (ae_std_dict_id);


--
-- TOC entry 2910 (class 2606 OID 16549)
-- Name: allergy_master allergy_master_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.allergy_master
    ADD CONSTRAINT allergy_master_pkey PRIMARY KEY (allergy_master_id);


--
-- TOC entry 2912 (class 2606 OID 16551)
-- Name: allergy_master allergy_name; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.allergy_master
    ADD CONSTRAINT allergy_name UNIQUE (allergy_name);


--
-- TOC entry 2916 (class 2606 OID 16582)
-- Name: contraindication contraindication_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contraindication
    ADD CONSTRAINT contraindication_pkey PRIMARY KEY (contraindication_id);


--
-- TOC entry 2918 (class 2606 OID 16591)
-- Name: day_limitation day_limitation_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.day_limitation
    ADD CONSTRAINT day_limitation_pkey PRIMARY KEY (day_limitation_id);


--
-- TOC entry 2896 (class 2606 OID 16517)
-- Name: dose dose_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.dose
    ADD CONSTRAINT dose_pkey PRIMARY KEY (dose_id);


--
-- TOC entry 2932 (class 2606 OID 16703)
-- Name: drug_basic_info drug_basic_info_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.drug_basic_info
    ADD CONSTRAINT drug_basic_info_pkey PRIMARY KEY (drug_basic_info_id);


--
-- TOC entry 2922 (class 2606 OID 16632)
-- Name: drug_identification drug_identification_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.drug_identification
    ADD CONSTRAINT drug_identification_pkey PRIMARY KEY (drug_identification_id);


--
-- TOC entry 2924 (class 2606 OID 16630)
-- Name: drug_identification drug_identification_yj_code; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.drug_identification
    ADD CONSTRAINT drug_identification_yj_code UNIQUE (yj_code);


--
-- TOC entry 2926 (class 2606 OID 16639)
-- Name: drug_price drug_price_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.drug_price
    ADD CONSTRAINT drug_price_pkey PRIMARY KEY (drug_price_id);


--
-- TOC entry 2876 (class 2606 OID 16459)
-- Name: early_symptom early_symptom_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.early_symptom
    ADD CONSTRAINT early_symptom_pkey PRIMARY KEY (early_symptom_id);


--
-- TOC entry 2880 (class 2606 OID 16473)
-- Name: efficacy_class_master efficacy_class_master_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_master
    ADD CONSTRAINT efficacy_class_master_pkey PRIMARY KEY (efficacy_class_id);


--
-- TOC entry 2882 (class 2606 OID 16475)
-- Name: efficacy_class_master efficacy_class_name; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_master
    ADD CONSTRAINT efficacy_class_name UNIQUE (efficacy_class_name);


--
-- TOC entry 2884 (class 2606 OID 16485)
-- Name: efficacy_class_std_dict efficacy_class_name_variation; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_std_dict
    ADD CONSTRAINT efficacy_class_name_variation UNIQUE (efficacy_class_name_variation);


--
-- TOC entry 2892 (class 2606 OID 16649)
-- Name: efficacy_class efficacy_class_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class
    ADD CONSTRAINT efficacy_class_pkey PRIMARY KEY (general_code, efficacy_class_id);


--
-- TOC entry 2888 (class 2606 OID 16653)
-- Name: efficacy_class_relation efficacy_class_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_relation
    ADD CONSTRAINT efficacy_class_relation_pkey PRIMARY KEY (parent_efficacy_class_id, child_efficacy_class_id);


--
-- TOC entry 2886 (class 2606 OID 16483)
-- Name: efficacy_class_std_dict efficacy_class_std_dict_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_std_dict
    ADD CONSTRAINT efficacy_class_std_dict_pkey PRIMARY KEY (efficacy_class_std_dict_id);


--
-- TOC entry 2860 (class 2606 OID 16657)
-- Name: general_master general_code; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.general_master
    ADD CONSTRAINT general_code UNIQUE (general_code);


--
-- TOC entry 2862 (class 2606 OID 16659)
-- Name: general_master general_master_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.general_master
    ADD CONSTRAINT general_master_pkey PRIMARY KEY (general_code);


--
-- TOC entry 2898 (class 2606 OID 16525)
-- Name: guidance_master guidance_master_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.guidance_master
    ADD CONSTRAINT guidance_master_pkey PRIMARY KEY (guidance_id);


--
-- TOC entry 2900 (class 2606 OID 16663)
-- Name: guidance guidance_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.guidance
    ADD CONSTRAINT guidance_pkey PRIMARY KEY (yj_code, guidance_id);


--
-- TOC entry 2904 (class 2606 OID 16539)
-- Name: high_risk_guidance_master high_risk_guidance_master_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.high_risk_guidance_master
    ADD CONSTRAINT high_risk_guidance_master_pkey PRIMARY KEY (high_risk_guidance_id);


--
-- TOC entry 2906 (class 2606 OID 16669)
-- Name: high_risk_guidance high_risk_guidance_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.high_risk_guidance
    ADD CONSTRAINT high_risk_guidance_pkey PRIMARY KEY (yj_code, high_risk_guidance_id);


--
-- TOC entry 2894 (class 2606 OID 16509)
-- Name: indication_dose_text indication_dose_text_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.indication_dose_text
    ADD CONSTRAINT indication_dose_text_pkey PRIMARY KEY (indication_dose_text_id);


--
-- TOC entry 2930 (class 2606 OID 16682)
-- Name: interaction interaction_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.interaction
    ADD CONSTRAINT interaction_pkey PRIMARY KEY (interaction_id);


--
-- TOC entry 2914 (class 2606 OID 16556)
-- Name: allergy uq_allergy; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.allergy
    ADD CONSTRAINT uq_allergy UNIQUE (yj_code, allergy_master_id);


--
-- TOC entry 2920 (class 2606 OID 16589)
-- Name: day_limitation uq_day_limitation; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.day_limitation
    ADD CONSTRAINT uq_day_limitation UNIQUE (dps_code, limitation_category);


--
-- TOC entry 2928 (class 2606 OID 16641)
-- Name: drug_price uq_dps_mod; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.drug_price
    ADD CONSTRAINT uq_dps_mod UNIQUE (dps_code, modified);


--
-- TOC entry 2878 (class 2606 OID 16461)
-- Name: early_symptom uq_early_symptom; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.early_symptom
    ADD CONSTRAINT uq_early_symptom UNIQUE (ae_extract_id, early_symptom_label);


--
-- TOC entry 2890 (class 2606 OID 16490)
-- Name: efficacy_class_relation uq_efficacy_class_relation; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.efficacy_class_relation
    ADD CONSTRAINT uq_efficacy_class_relation UNIQUE (parent_efficacy_class_id, child_efficacy_class_id);


--
-- TOC entry 2902 (class 2606 OID 16661)
-- Name: guidance uq_guidance; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.guidance
    ADD CONSTRAINT uq_guidance UNIQUE (yj_code, guidance_id);


--
-- TOC entry 2908 (class 2606 OID 16667)
-- Name: high_risk_guidance uq_high_risk_guidance; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.high_risk_guidance
    ADD CONSTRAINT uq_high_risk_guidance UNIQUE (yj_code, high_risk_guidance_id);


--
-- TOC entry 2864 (class 2606 OID 16686)
-- Name: yj_codes yj_code; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.yj_codes
    ADD CONSTRAINT yj_code UNIQUE (yj_code);


--
-- TOC entry 2866 (class 2606 OID 16688)
-- Name: yj_codes yj_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.yj_codes
    ADD CONSTRAINT yj_codes_pkey PRIMARY KEY (yj_code);


--
-- TOC entry 2868 (class 2606 OID 16692)
-- Name: yj_transition yj_transition_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.yj_transition
    ADD CONSTRAINT yj_transition_pkey PRIMARY KEY (old_yj_code);


-- Completed on 2021-01-04 11:28:55 UTC

--
-- PostgreSQL database dump complete
--

