--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-2.pgdg90+1)
-- Dumped by pg_dump version 10.5

-- Started on 2021-01-04 11:12:13 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3057 (class 0 OID 16438)
-- Dependencies: 199
-- Data for Name: ae_extract_dict; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.ae_extract_dict (ae_extract_id, ae_extract_name) FROM stdin;
\.


--
-- TOC entry 3058 (class 0 OID 16445)
-- Dependencies: 200
-- Data for Name: ae_std_dict; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.ae_std_dict (ae_std_dict_id, ae_extract_id, ae_extract_name_variation) FROM stdin;
\.


--
-- TOC entry 3071 (class 0 OID 16552)
-- Dependencies: 213
-- Data for Name: allergy; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.allergy (yj_code, allergy_master_id) FROM stdin;
\.


--
-- TOC entry 3070 (class 0 OID 16545)
-- Dependencies: 212
-- Data for Name: allergy_master; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.allergy_master (allergy_master_id, allergy_name) FROM stdin;
\.


--
-- TOC entry 3075 (class 0 OID 16575)
-- Dependencies: 217
-- Data for Name: contraindication; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.contraindication (contraindication_id, yj_code, subject, patient_property, warning_level, description) FROM stdin;
\.


--
-- TOC entry 3077 (class 0 OID 16585)
-- Dependencies: 219
-- Data for Name: day_limitation; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.day_limitation (day_limitation_id, dps_code, limitation_category, limitation_days, expiration_date) FROM stdin;
\.


--
-- TOC entry 3065 (class 0 OID 16510)
-- Dependencies: 207
-- Data for Name: dose; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.dose (dose_id, yj_code, route, treatment, dose) FROM stdin;
\.


--
-- TOC entry 3093 (class 0 OID 16696)
-- Dependencies: 235
-- Data for Name: drug_basic_info; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.drug_basic_info (drug_basic_info_id, updated_date, yj_code, dps_registration_date, launch_date, route, strength, maker_name, generic_drug, brand_drug, brand_drug_available, expiration_date, shusai_date, regulation, ingredients, high_risk, basic_drug, alterable) FROM stdin;
1	2019-07-01	1124022F1067	\N	\N	内用薬	０．５ｍｇ１錠	武田薬品	f	t	f	\N	\N	向精神薬	\N	f	\N	\N
2	2019-07-01	1129009S1023	\N	\N	内用薬	５ｍｇ１ｍＬ１包	高田製薬	t	f	f	\N	\N	向精神薬	\N	f	\N	\N
3	2020-10-01	1319751Q1020	\N	\N	外用薬	０．３％１ｍＬ	大塚製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
4	2017-10-01	2249003B1037	\N	\N	内用薬	１０％１ｇ	ニプロＥＳファーマ	f	t	f	\N	\N	\N	\N	f	\N	\N
5	2020-06-01	2329028F1031	\N	\N	内用薬	１０ｍｇ１錠	武田薬品	t	f	f	\N	\N	\N	\N	f	\N	\N
6	2018-11-01	2359006M1025	\N	\N	内用薬	２４μｇ１カプセル	アボットジャパン	f	t	f	\N	\N	\N	\N	f	\N	\N
7	2020-10-01	2452002F1030	\N	\N	内用薬	１０ｍｇ１錠	ファイザー	f	t	f	\N	\N	\N	\N	t	\N	\N
8	2020-01-01	2639700Q1039	\N	\N	外用薬	１％１ｍＬ	サンファーマ	t	f	f	\N	\N	\N	\N	f	\N	\N
9	2014-03-01	2649734S1040	\N	\N	外用薬	７ｃｍ×１０ｃｍ１枚	テイコクメディックス	t	f	f	\N	\N	\N	\N	f	\N	\N
10	2019-03-01	3999016M1021	\N	\N	内用薬	２ｍｇ１カプセル	武田薬品	f	t	f	\N	\N	\N	\N	t	\N	\N
11	2019-11-01	3999453G1021	\N	\N	注射薬	５μｇ０．５ｍＬ１筒	キッセイ	t	f	f	\N	\N	\N	\N	f	\N	\N
12	2015-02-01	4240400D1030	\N	\N	注射薬	１ｍｇ１瓶	日本化薬	f	t	f	\N	\N	\N	\N	t	\N	\N
13	\N	4291031M1024	\N	\N	\N	４０ｍｇ１カプセル	アステラス製薬	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
14	\N	4291407A1027	\N	\N	\N	１００ｍｇ１０ｍＬ１瓶	全薬工業	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
15	2005-10-01	5200035D1022	\N	\N	内用薬	１ｇ	大杉製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
16	2020-10-01	6133401G1022	\N	\N	注射薬	１ｇ１キット（生理食塩液１００ｍＬ付）	塩野義	f	t	f	\N	\N	\N	\N	f	\N	\N
17	2020-09-01	6149003R1046	\N	\N	内用薬	１００ｍｇ１ｇ	エルメッド	t	f	f	\N	\N	\N	\N	f	\N	\N
18	2020-02-01	6250042F1020	\N	\N	内用薬	４００ｍｇ１錠	ギリアド・サイエンシズ	f	t	f	\N	\N	\N	\N	f	\N	\N
19	2020-10-01	631340GD1030	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
20	2020-02-01	8114003F1035	\N	\N	内用薬	１０ｍｇ１錠	第一三共	f	t	f	\N	\N	麻薬	\N	f	\N	\N
21	2020-02-01	1115400X1027	\N	\N	注射薬	３００ｍｇ１管	田辺三菱製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
22	2019-04-01	1139100F1037	\N	\N	内用薬	１錠	大日本住友製薬	f	t	f	\N	\N	向精神薬	\N	t	\N	\N
23	2016-07-01	1147002N1123	\N	\N	内用薬	３７．５ｍｇ１カプセル	エスエス	f	t	f	\N	\N	\N	\N	f	\N	\N
24	2013-05-01	1149108D1039	\N	\N	内用薬	１ｇ	杏林製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
25	2019-07-01	1169001F1024	\N	\N	内用薬	２ｍｇ１錠	武田薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
26	2018-04-01	1172005F1044	\N	\N	内用薬	５ｍｇ１錠	高田製薬	f	t	f	\N	\N	\N	\N	t	\N	\N
27	2019-07-01	1179012F1118	\N	\N	内用薬	５ｍｇ１錠	三菱ウェルファーマ	f	t	f	\N	\N	向精神薬	\N	t	\N	\N
28	2020-09-01	1179038S1030	\N	\N	内用薬	０．１％１ｍＬ	共和薬工	t	f	f	\N	\N	\N	\N	t	\N	\N
29	2020-02-01	1179408E1020	\N	\N	注射薬	１０ｍｇ１瓶	日本イーライリリー	f	t	f	\N	\N	\N	\N	t	\N	\N
30	2020-08-01	1190025F1023	\N	\N	内用薬	１０ｍｇ１錠	大塚製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
31	2016-04-01	1219002D1048	\N	\N	内用薬	５％１ｇ	エーザイ	f	t	f	2021-03-31	\N	\N	\N	f	\N	\N
32	2019-08-01	1242003F2021	\N	\N	内用薬	１０ｍｇ１錠	エーザイ	f	t	f	\N	\N	\N	\N	f	\N	\N
33	2019-07-01	1312704Q1024	\N	\N	外用薬	０．５％１ｍＬ	鳥居薬品	f	t	f	\N	\N	毒薬	\N	f	\N	\N
34	2016-08-01	1319714Q3099	\N	\N	外用薬	０．０５％５ｍＬ１瓶	参天製薬	t	f	f	\N	\N	\N	\N	f	\N	\N
35	2011-07-01	1324701R1035	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
36	2012-12-01	2115001X1104	\N	\N	内用薬	１ｇ	エーザイ	f	t	f	\N	\N	\N	\N	t	\N	\N
37	2020-08-01	2119409D1033	\N	\N	注射薬	５ｍｇ１瓶	日本化薬	f	t	f	\N	\N	毒薬	\N	f	\N	\N
38	2010-11-01	2129404A2037	\N	\N	注射薬	１００ｍｇ１０ｍＬ１管	日本シェーリング	f	t	f	\N	\N	\N	\N	t	\N	\N
39	\N	2143006F2056	\N	\N	\N	０．２５ｍｇ１錠	第一三共	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
40	2013-01-01	2149020N1031	\N	\N	内用薬	１５ｍｇ１カプセル	科研製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
41	2018-04-01	2149118F1020	\N	\N	内用薬	１錠	大日本住友製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
42	2018-05-01	2171014N1029	\N	\N	内用薬	１０ｍｇ１カプセル	シェリングＰ	f	t	f	\N	\N	\N	\N	f	\N	\N
43	2007-12-01	2189006M1058	\N	\N	内用薬	２５０ｍｇ１カプセル	アルフレッサ　ファーマ	t	f	f	\N	\N	\N	\N	f	\N	\N
44	2019-01-01	2190021B1095	\N	\N	内用薬	１％１ｇ	田辺三菱製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
45	2019-04-01	2190411F1037	\N	\N	注射薬	１ｍｇ１瓶	ＭＳＤ	f	t	f	\N	\N	\N	\N	f	\N	\N
46	2019-07-01	2229100B1110	\N	\N	内用薬	１ｇ	日医工	t	f	f	\N	\N	\N	\N	f	\N	\N
47	2017-10-01	2259700G1034	\N	\N	外用薬	０．５％１ｍＬ	ニプロＥＳファーマ	f	t	f	\N	\N	\N	\N	f	\N	\N
48	2019-04-01	2316014F1023	\N	\N	内用薬	１錠	興和	f	t	f	\N	\N	\N	\N	f	\N	\N
49	2010-06-01	2325401D1086	\N	\N	注射薬	２０ｍｇ１管	沢井製薬	t	f	f	\N	\N	\N	\N	f	\N	\N
50	2006-03-01	2339106B1046	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
51	2019-04-01	2339266B1020	\N	\N	内用薬	１ｇ	大正富山医薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
52	\N	2399005F1043	\N	\N	\N	５ｍｇ１錠	辰巳化学	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
53	2020-06-01	2399715X1023	\N	\N	外用薬	１ｇ１個	杏林製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
54	2013-12-01	2478001F1040	\N	\N	内用薬	２ｍｇ１錠	塩野義	f	t	f	\N	\N	\N	\N	f	\N	\N
55	2020-06-01	2499005F1030	\N	\N	内用薬	０．５ｍｇ１錠	科研製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
56	2019-10-01	2519700Q1030	\N	\N	外用薬	３％３Ｌ１袋	バクスター	f	t	f	\N	\N	\N	\N	f	\N	\N
57	2009-10-01	2590003F1023	\N	\N	内用薬	１錠	扶桑薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
58	2005-04-01	261270BS1023	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
59	2008-03-01	2619714X1059	\N	\N	\N	１ｇ	健栄製薬	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
60	2015-04-01	2646709Q1034	\N	\N	外用薬	０．０１％１ｍＬ	田辺三菱製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
61	2015-04-01	2646729M1048	\N	\N	外用薬	０．３％１ｇ	鳥居薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
62	2020-06-01	264970AM1020	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
63	2015-04-01	2655707N1038	\N	\N	外用薬	１％１ｇ	田辺三菱製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
64	2020-06-01	2669700X1040	\N	\N	外用薬	１０ｍＬ	丸石製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
65	2016-11-01	2710803U1044	\N	\N	歯科用薬	１．８ｍＬ１管	昭和薬品化工	t	f	f	\N	\N	\N	\N	f	\N	\N
66	2016-04-01	279080BF1041	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
67	2015-08-01	3122001F1027	\N	\N	内用薬	２５ｍｇ１錠	高田製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
68	2016-09-01	3135400A1026	\N	\N	注射薬	１５ｍｇ１管	武田薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
69	2020-09-01	3179110M1034	\N	\N	内用薬	１カプセル	第一三共	f	t	f	\N	\N	\N	\N	f	\N	\N
70	2014-10-01	3222400A1058	\N	\N	注射薬	４０ｍｇ２ｍＬ１管	日医工	f	t	f	\N	\N	\N	\N	f	\N	\N
71	2020-06-01	3239502X1020	\N	\N	注射薬	７００ｍＬ１袋	テルモ	f	t	f	\N	\N	\N	\N	f	\N	\N
72	2019-12-01	3259119S1029	\N	\N	内用薬	１０ｍＬ	アボットジャパン	f	t	f	\N	\N	\N	\N	f	\N	\N
73	2016-12-01	3311402G1027	\N	\N	注射薬	１００ｍＬ１キット	大塚製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
74	2011-03-01	3319539A1082	\N	\N	注射薬	５００ｍＬ１瓶	扶桑薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
75	2017-02-01	3327401G1030	\N	\N	注射薬	１０％１０ｍＬ１筒	ニプロ	t	f	f	\N	\N	\N	\N	f	\N	\N
76	2017-09-01	3339950Q1074	\N	\N	外用薬	１ｇ	マルホ	f	t	f	\N	\N	\N	\N	f	\N	\N
77	2020-09-01	3410521A1031	\N	\N	注射薬	９Ｌ１瓶（炭酸水素ナトリウム付）	陽進堂	f	t	f	\N	\N	\N	\N	f	\N	\N
78	2014-12-01	3420432A1038	\N	\N	注射薬	１．５Ｌ１袋	バクスター	f	t	f	\N	\N	\N	\N	f	\N	\N
79	2020-05-01	3929009M1027	\N	\N	内用薬	５０ｍｇ１カプセル	マイラン製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
80	2018-11-01	3959412F1021	\N	\N	注射薬	５０ｍｇ１瓶	ジェンザイム・ジャパン	f	t	f	\N	\N	\N	\N	f	\N	\N
81	\N	39690A7F1028	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N
82	2019-07-01	3999411A1033	\N	\N	注射薬	２０ｍｇ２ｍＬ１管	興和テバ	t	f	f	2021-03-31	\N	\N	\N	f	\N	\N
83	2019-07-01	4223701M1047	\N	\N	外用薬	５％１ｇ	協和キリン	f	t	f	\N	\N	\N	\N	t	\N	\N
84	2019-08-01	4299100F1026	\N	\N	内用薬	１５ｍｇ１錠（トリフルリジン相当量）	大鵬薬品	f	t	f	\N	\N	\N	\N	t	\N	\N
85	2014-12-01	4300438A1023	\N	\N	注射薬	１０ＭＢｑ	日本メジフィジックス	f	t	f	\N	\N	\N	\N	f	\N	\N
86	2019-06-01	4419005B1045	\N	\N	内用薬	１％１ｇ	ＭＳＤ	f	t	f	\N	\N	\N	\N	f	\N	\N
87	2019-04-01	4490017F1029	\N	\N	内用薬	１１２．５ｍｇ１錠	エルメッド	t	f	f	\N	\N	\N	\N	f	\N	\N
88	2019-07-01	4490403A1030	\N	\N	注射薬	２ｍＬ１瓶	鳥居薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
89	2018-10-01	5200014C1025	\N	\N	内用薬	１ｇ	大杉製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
90	2005-10-01	5200051D1021	\N	\N	内用薬	１ｇ	大杉製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
91	2009-06-01	5200070D1024	\N	\N	内用薬	１ｇ	大杉製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
92	2013-03-01	5200091D1026	\N	\N	内用薬	１ｇ	ツムラ	f	t	f	\N	\N	\N	\N	f	\N	\N
93	2013-03-01	5200113D1024	\N	\N	内用薬	１ｇ	ツムラ	f	t	f	\N	\N	\N	\N	f	\N	\N
94	2005-10-01	5200129D1025	\N	\N	内用薬	１ｇ	大杉製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
95	2008-05-01	5200144C1024	\N	\N	内用薬	１ｇ	小太郎漢方製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
96	2020-04-01	6123401A1031	\N	\N	注射薬	１０ｍｇ１管	ジェイドルフ製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
97	2020-09-01	6132009F1023	\N	\N	内用薬	５０ｍｇ１錠	大正富山医薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
98	2020-10-01	6199104X1023	\N	\N	内用薬	１シート	武田薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
99	2020-01-01	6241019F1021	\N	\N	内用薬	７５ｍｇ１錠	杏林製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
100	2020-08-01	6290002M1020	\N	\N	内用薬	５０ｍｇ１カプセル	ファイザー	f	t	f	\N	\N	\N	\N	f	\N	\N
101	2019-12-01	6343400X1041	\N	\N	注射薬	５００ｍｇ１０ｍＬ１瓶（溶解液付）	ＫＭバイオロジクス	f	t	f	\N	\N	\N	\N	f	\N	\N
102	2019-05-01	6343442D1022	\N	\N	注射薬	２５０国際単位１瓶（溶解液付）	バイオジェン・ジャパン	f	t	f	\N	\N	\N	\N	f	\N	\N
103	2018-10-01	6399500D2037	\N	\N	注射薬	（人免疫グロブリン１２ｍｇヒスタミン二塩酸塩０．１５μｇ）１瓶（溶解液付）	日本臓器	f	t	f	\N	\N	\N	\N	f	\N	\N
104	2015-06-01	7121709X1094	\N	\N	外用薬	１０ｇ	東豊薬品	f	t	f	\N	\N	\N	\N	f	\N	\N
105	2019-04-01	7212010X1029	\N	\N	内用薬	９７．９８％１０ｇ	伏見製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
106	2020-07-01	7219415G1041	\N	\N	注射薬	５１．７７％１００ｍＬ１筒	富士製薬工業	f	t	f	2021-03-31	\N	\N	\N	f	\N	\N
107	2015-01-01	7290004B1030	\N	\N	内用薬	６００ｍｇ１包	大塚製薬	f	t	f	\N	\N	\N	\N	f	\N	\N
108	2012-04-01	7319700Q1032	\N	\N	外用薬	１０ｍＬ	小堺製薬	t	f	f	\N	\N	\N	\N	f	\N	\N
109	2020-02-01	8219502A1028	\N	\N	注射薬	１ｍＬ１管	塩野義	f	t	f	\N	\N	麻薬	\N	f	\N	\N
\.


--
-- TOC entry 3081 (class 0 OID 16623)
-- Dependencies: 223
-- Data for Name: drug_identification; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.drug_identification (drug_identification_id, modified, yj_code, imprint, packaging_identifier, color, splittable, form, manufacturer, producer, seller, appearance, maker_mark) FROM stdin;
\.


--
-- TOC entry 3083 (class 0 OID 16635)
-- Dependencies: 225
-- Data for Name: drug_price; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.drug_price (drug_price_id, modified, dps_code, price) FROM stdin;
1	2020-04-01	1124022F1067	5.90
2	2020-04-01	1129009S1023	60.40
3	2020-04-01	1319751Q1020	140.00
4	2020-04-01	2249003B1037	9.30
5	2020-04-01	2329028F1031	46.30
6	2020-04-01	2359006M1025	120.40
7	2020-04-01	2452002F1030	7.40
8	2020-04-01	2639700Q1012	26.90
9	2020-12-11	2639700Q1012	26.90
10	2020-04-01	2649734S1015	11.10
11	2020-04-01	3999016M1021	210.10
12	2020-04-01	3999453G1021	826.00
13	2020-04-01	4240400D1030	2521.00
14	2020-04-01	5200035D1022	6.50
15	2020-04-01	6133401G1022	1616.00
16	2020-04-01	6149003R1046	40.10
17	2020-04-01	6250042F1020	43014.60
18	2020-04-01	8114003F1019	128.10
19	2020-04-01	1115400X1019	841.00
20	2020-04-01	1139100F1037	5.90
21	2020-04-01	1147002N1123	14.20
22	2020-04-01	1149108D1039	11.30
23	2020-04-01	1169001F1024	8.80
24	2020-04-01	1172005F1044	5.70
25	2020-04-01	1179012F1118	6.40
26	2020-04-01	1179038S1013	29.20
27	2020-04-01	1179408E1020	1962.00
28	2020-04-01	1190025F1023	301.50
29	2020-04-01	1219002D1048	32.60
30	2020-04-01	1242003F2021	14.30
31	2020-04-01	1312704Q1024	137.60
32	2020-04-01	1319714Q3099	86.40
33	2020-04-01	2115001X1015	11.40
34	2020-04-01	2119409D1033	4011.00
35	2020-04-01	2129404A2037	743.00
36	2020-04-01	2143006F2013	9.80
37	2020-04-01	2149020N1031	15.90
38	2020-04-01	2149118F1020	105.30
39	2020-04-01	2171014N1029	12.50
40	2020-04-01	2189006M1058	7.70
41	2020-04-01	2190021B1095	34.30
42	2020-04-01	2190411F1037	6624.00
43	2020-04-01	2229100B1110	6.50
44	2020-04-01	2259700G1034	29.00
45	2020-04-01	2316014F1023	5.90
46	2020-04-01	2325401D1019	97.00
47	2020-04-01	2339266B1020	6.30
48	2020-04-01	2399715X1023	599.20
49	2020-04-01	2478001F1040	25.00
50	2020-04-01	2499005F1030	268.30
51	2020-04-01	2519700Q1030	1020.70
52	2020-04-01	2590003F1023	14.70
53	2020-04-01	2646709Q1034	20.60
54	2020-04-01	2646729M1048	19.30
55	2020-04-01	2655707N1038	21.90
56	2020-04-01	2669700X1016	2.37
57	2020-04-01	2710803U1044	78.40
58	2020-04-01	3122001F1027	5.90
59	2020-04-01	3135400A1018	96.00
60	2020-04-01	3179110M1034	8.00
61	2020-04-01	3222400A1058	60.00
62	2020-04-01	3239502X1020	456.00
63	2020-04-01	3259119S1029	0.73
64	2020-04-01	3311402G1027	149.00
65	2020-04-01	3319539A1015	219.00
66	2020-04-01	3327401G1030	155.00
67	2020-04-01	3339950Q1074	21.60
68	2020-04-01	3410521A1031	1294.00
69	2020-04-01	3420432A1038	727.00
70	2020-04-01	3929009M1027	219.40
71	2020-04-01	3959412F1021	98470.00
72	2020-06-19	3999411A1033	387.00
73	2020-04-01	4223701M1047	323.50
74	2020-04-01	4299100F1026	2516.90
75	2020-04-01	4300438A1023	318.00
76	2020-04-01	4419005B1045	6.50
77	2020-04-01	4490017F1010	21.40
78	2020-04-01	4490403A1030	4375.00
79	2020-04-01	5200014C1025	9.00
80	2020-04-01	5200051D1021	15.20
81	2020-04-01	5200070D1024	13.20
82	2020-04-01	5200091D1026	8.00
83	2020-04-01	5200113D1024	11.60
84	2020-04-01	5200129D1025	6.50
85	2020-04-01	5200144C1024	9.90
86	2020-04-01	6123401A1031	97.00
87	2020-04-01	6132009F1023	31.80
88	2020-04-01	6199104X1023	657.30
89	2020-04-01	6241019F1021	353.40
90	2020-04-01	6290002M1020	362.10
91	2020-04-01	6343400X1041	4319.00
92	2020-04-01	6343442D1022	24803.00
93	2020-04-01	6399500D2037	731.00
94	2020-04-01	7121709X1094	2.61
95	2020-04-01	7212010X1029	1.44
96	2020-12-11	7219415G1041	4434.00
97	2020-04-01	7290004B1030	863.50
98	2020-04-01	7319700Q1032	2.21
99	2020-04-01	8219502A1028	357.00
\.


--
-- TOC entry 3059 (class 0 OID 16452)
-- Dependencies: 201
-- Data for Name: early_symptom; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.early_symptom (early_symptom_id, ae_extract_id, early_symptom_label, early_symptom_content) FROM stdin;
\.


--
-- TOC entry 3063 (class 0 OID 16491)
-- Dependencies: 205
-- Data for Name: efficacy_class; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.efficacy_class (general_code, efficacy_class_id) FROM stdin;
\.


--
-- TOC entry 3060 (class 0 OID 16469)
-- Dependencies: 202
-- Data for Name: efficacy_class_master; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.efficacy_class_master (efficacy_class_id, efficacy_class_name) FROM stdin;
\.


--
-- TOC entry 3062 (class 0 OID 16486)
-- Dependencies: 204
-- Data for Name: efficacy_class_relation; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.efficacy_class_relation (parent_efficacy_class_id, child_efficacy_class_id) FROM stdin;
\.


--
-- TOC entry 3061 (class 0 OID 16476)
-- Dependencies: 203
-- Data for Name: efficacy_class_std_dict; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.efficacy_class_std_dict (efficacy_class_std_dict_id, efficacy_class_id, efficacy_class_name_variation) FROM stdin;
\.


--
-- TOC entry 3054 (class 0 OID 16385)
-- Dependencies: 196
-- Data for Name: general_master; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.general_master (general_code, general_name, general_name_ruby, active_ingredients) FROM stdin;
\.


--
-- TOC entry 3067 (class 0 OID 16527)
-- Dependencies: 209
-- Data for Name: guidance; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.guidance (yj_code, guidance_id) FROM stdin;
\.


--
-- TOC entry 3066 (class 0 OID 16518)
-- Dependencies: 208
-- Data for Name: guidance_master; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.guidance_master (guidance_id, guidance_content, guidance_category, guidance_category2) FROM stdin;
\.


--
-- TOC entry 3069 (class 0 OID 16540)
-- Dependencies: 211
-- Data for Name: high_risk_guidance; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.high_risk_guidance (yj_code, high_risk_guidance_id) FROM stdin;
\.


--
-- TOC entry 3068 (class 0 OID 16532)
-- Dependencies: 210
-- Data for Name: high_risk_guidance_master; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.high_risk_guidance_master (high_risk_guidance_id, high_risk_guidance_content, high_risk_guidance_category, high_risk_guidance_category_aux) FROM stdin;
\.


--
-- TOC entry 3064 (class 0 OID 16502)
-- Dependencies: 206
-- Data for Name: indication_dose_text; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.indication_dose_text (indication_dose_text_id, yj_code, indication, dosage) FROM stdin;
\.


--
-- TOC entry 3091 (class 0 OID 16674)
-- Dependencies: 233
-- Data for Name: interaction; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.interaction (interaction_id, general_code_left, general_code_right, warning_level, treatment, interaction_factor) FROM stdin;
\.


--
-- TOC entry 3055 (class 0 OID 16393)
-- Dependencies: 197
-- Data for Name: yj_codes; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.yj_codes (yj_code, dps_code, individual_receipt_code, generic_receipt_code, general_code, product_name, product_name_ruby) FROM stdin;
1124022F1067	1124022F1067	611170470	<NA>     	\N	ワイパックス錠０．５　０．５ｍｇ	ﾜｲﾊﾟｯｸｽｼﾞｮｳ0.5
1129009S1023	1129009S1023	622145301	<NA>     	\N	ゾルピデム酒石酸塩内用液５ｍｇ「タカタ」　１ｍＬ	ｿﾞﾙﾋﾟﾃﾞﾑｼｭｾｷｻﾝｴﾝﾅｲﾖｳ
1319751Q1020	1319751Q1020	620003475	<NA>     	\N	オゼックス点眼液０．３％	ｵｾﾞｯｸｽﾃﾝｶﾞﾝｴｷ0.3%
2249003B1037	2249003B1037	620002500	<NA>     	\N	アスベリン散１０％	ｱｽﾍﾞﾘﾝｻﾝ10%
2329028F1031	2329028F1031	622026001	<NA>     	\N	ラベプラゾールＮａ錠１０ｍｇ「ＡＡ」	ﾗﾍﾞﾌﾟﾗｿﾞｰﾙNAｼﾞｮｳ10MG
2359006M1025	2359006M1025	622182901	<NA>     	\N	アミティーザカプセル２４μｇ	ｱﾐﾃｨｰｻﾞｶﾌﾟｾﾙ24ﾏｲｸﾛG
2452002F1030	2452002F1030	620006903	<NA>     	\N	コートリル錠１０ｍｇ	ｺｰﾄﾘﾙｼﾞｮｳ10MG
2639700Q1039	2639700Q1012	620003047	622775700	\N	ナジフロローション１％	ﾅｼﾞﾌﾛﾛｰｼｮﾝ1%
2649734S1040	2649734S1015	620005738	622780400	\N	ジクロフェナクナトリウムテープ１５ｍｇ「テイコク」　７×１０ｃｍ	ｼﾞｸﾛﾌｪﾅｸﾅﾄﾘｳﾑﾃｰﾌﾟ15M
3999016M1021	3999016M1021	610432016	<NA>     	\N	リウマトレックスカプセル２ｍｇ	ﾘｳﾏﾄﾚｯｸｽｶﾌﾟｾﾙ2MG
3999453G1021	3999453G1021	629902301	<NA>     	\N	ダルベポエチン　アルファＢＳ注５μｇシリンジＪＣＲ　０．５ｍＬ	ﾀﾞﾙﾍﾞﾎﾟｴﾁﾝｱﾙﾌｧBSﾁｭｳ5
4240400D1030	4240400D1030	640454006	<NA>     	\N	オンコビン注射用１ｍｇ	ｵﾝｺﾋﾞﾝﾁｭｳｼｬﾖｳ1MG
4291031M1024	<NA>        	622335901	<NA>     	\N	<NA>	<NA>
4291407A1027	<NA>        	640451030	<NA>     	\N	<NA>	<NA>
5200035D1022	5200035D1022	615101028	<NA>     	\N	オースギ桂枝加竜骨牡蛎湯エキスＧ	ｵｰｽｷﾞｹｲｼｶﾘｭｳｺﾂﾎﾞﾚｲﾄｳ
6133401G1022	6133401G1022	640443048	<NA>     	\N	フルマリンキット静注用１ｇ　（生理食塩液１００ｍＬ付）	ﾌﾙﾏﾘﾝｷｯﾄｼﾞｮｳﾁｭｳﾖｳ1G
6149003R1046	6149003R1046	620003940	<NA>     	\N	クラリスロマイシンＤＳ１０％小児用「ＥＭＥＣ」　１００ｍｇ	ｸﾗﾘｽﾛﾏｲｼﾝDS10%ｼｮｳﾆﾖｳ
6250042F1020	6250042F1020	622418801	<NA>     	\N	ソバルディ錠４００ｍｇ	ｿﾊﾞﾙﾃﾞｨｼﾞｮｳ400MG
631340GD1030	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
8114003F1035	8114003F1019	620009255	618110017	\N	モルヒネ塩酸塩錠１０ｍｇ「ＤＳＰ」	ﾓﾙﾋﾈｴﾝｻﾝｴﾝｼﾞｮｳ10MGDS
1115400X1027	1115400X1019	641110021	641110009	\N	ラボナール注射用０．３ｇ　３００ｍｇ	ﾗﾎﾞﾅｰﾙﾁｭｳｼｬﾖｳ0.3G
1139100F1037	1139100F1037	620066901	<NA>     	\N	複合アレビアチン配合錠	ﾌｸｺﾞｳｱﾚﾋﾞｱﾁﾝﾊｲｺﾞｳｼﾞｮ
1147002N1123	1147002N1123	610454052	<NA>     	\N	ナボールＳＲカプセル３７．５　３７．５ｍｇ	ﾅﾎﾞｰﾙSRｶﾌﾟｾﾙ37.5
1149108D1039	1149108D1039	620107901	<NA>     	\N	キョーリンＡＰ２配合顆粒	ｷｮｰﾘﾝAP2ﾊｲｺﾞｳｶﾘｭｳ
1169001F1024	1169001F1024	611240415	<NA>     	\N	アーテン錠（２ｍｇ）	ｱｰﾃﾝｼﾞｮｳ2MG
1172005F1044	1172005F1044	611170268	<NA>     	\N	ニューレプチル錠５ｍｇ	ﾆｭｰﾚﾌﾟﾁﾙｼﾞｮｳ5MG
1179012F1118	1179012F1118	610422300	<NA>     	\N	リーゼ錠５ｍｇ	ﾘｰｾﾞｼﾞｮｳ5MG
1179038S1030	1179038S1013	620005633	622715200	\N	リスペリドン内用液１ｍｇ／ｍＬ「アメル」　０．１％	ﾘｽﾍﾟﾘﾄﾞﾝﾅｲﾖｳｴｷ1MG/ML
1179408E1020	1179408E1020	622210701	<NA>     	\N	ジプレキサ筋注用１０ｍｇ	ｼﾞﾌﾟﾚｷｻｷﾝﾁｭｳﾖｳ10MG
1190025F1023	1190025F1023	622607601	<NA>     	\N	セリンクロ錠１０ｍｇ	ｾﾘﾝｸﾛｼﾞｮｳ10MG
1219002D1048	1219002D1048	620005990	<NA>     	\N	ストロカイン顆粒５％	ｽﾄﾛｶｲﾝｶﾘｭｳ5%
1242003F2021	1242003F2021	611240064	<NA>     	\N	コリオパン錠１０ｍｇ	ｺﾘｵﾊﾟﾝｼﾞｮｳ10MG
1312704Q1024	1312704Q1024	661310029	<NA>     	\N	ウブレチド点眼液０．５％	ｳﾌﾞﾚﾁﾄﾞﾃﾝｶﾞﾝｴｷ0.5%
1319714Q3099	1319714Q3099	620003844	<NA>     	\N	ＦＡＤ点眼液０．０５％「サンテン」　５ｍＬ	FADﾃﾝｶﾞﾝｴｷ0.05%ｻﾝﾃﾝ
1324701R1035	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
2115001X1104	2115001X1015	620005795	612110009	\N	ネオフィリン原末	ﾈｵﾌｨﾘﾝｹﾞﾝﾏﾂ
2119409D1033	2119409D1033	620248901	<NA>     	\N	アデール点滴静注用５ｍｇ	ｱﾃﾞｰﾙﾃﾝﾃｷｼﾞｮｳﾁｭｳﾖｳ5M
2129404A2037	2129404A2037	620004636	<NA>     	\N	アスペノン静注用１００　１００ｍｇ１０ｍＬ	ｱｽﾍﾟﾉﾝｼﾞｮｳﾁｭｳﾖｳ100
2143006F2056	<NA>        	620008285	612140317	\N	<NA>	<NA>
2149020N1031	2149020N1031	620005935	<NA>     	\N	エブランチルカプセル１５ｍｇ	ｴﾌﾞﾗﾝﾁﾙｶﾌﾟｾﾙ15MG
2149118F1020	2149118F1020	622199201	<NA>     	\N	アイミクス配合錠ＬＤ	ｱｲﾐｸｽﾊｲｺﾞｳｼﾞｮｳLD
2171014N1029	2171014N1029	612170547	<NA>     	\N	セパミット－Ｒカプセル１０　１０ｍｇ	ｾﾊﾟﾐｯﾄ-Rｶﾌﾟｾﾙ10
2189006M1058	2189006M1058	620005920	<NA>     	\N	ＥＰＬカプセル２５０ｍｇ	EPLｶﾌﾟｾﾙ250MG
2190021B1095	2190021B1095	620003547	<NA>     	\N	サアミオン散１％	ｻｱﾐｵﾝｻﾝ1%
2190411F1037	2190411F1037	620002565	<NA>     	\N	インダシン静注用１ｍｇ	ｲﾝﾀﾞｼﾝｼﾞｮｳﾁｭｳﾖｳ1MG
2229100B1110	2229100B1110	620379101	<NA>     	\N	ニチコデ配合散	ﾆﾁｺﾃﾞﾊｲｺﾞｳｻﾝ
2259700G1034	2259700G1034	620007594	<NA>     	\N	イノリン吸入液０．５％	ｲﾉﾘﾝｷｭｳﾆｭｳｴｷ0.5%
2316014F1023	2316014F1023	622113001	<NA>     	\N	ラックビー錠	ﾗｯｸﾋﾞｰｼﾞｮｳ
2325401D1086	2325401D1019	620003789	622755100	\N	ファモチジン注射用２０ｍｇ「サワイ」	ﾌｧﾓﾁｼﾞﾝﾁｭｳｼｬﾖｳ20MGｻﾜ
2339106B1046	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
2339266B1020	2339266B1020	622209401	<NA>     	\N	つくしＡ・Ｍ配合散	ﾂｸｼAMﾊｲｺﾞｳｻﾝ
2399005F1043	<NA>        	610407368	<NA>     	\N	<NA>	<NA>
2399715X1023	2399715X1023	660470002	<NA>     	\N	ペンタサ注腸１ｇ	ﾍﾟﾝﾀｻﾁｭｳﾁｮｳ1G
2478001F1040	2478001F1040	620005136	<NA>     	\N	ルトラール錠２ｍｇ	ﾙﾄﾗｰﾙｼﾞｮｳ2MG
2499005F1030	2499005F1030	620006109	<NA>     	\N	プロスタグランジンＥ２錠０．５ｍｇ「科研」	ﾌﾟﾛｽﾀｸﾞﾗﾝｼﾞﾝE2ｼﾞｮｳ0.
2519700Q1030	2519700Q1030	620556801	<NA>     	\N	ウロマチックＳ泌尿器科用灌流液３％　３Ｌ	ｳﾛﾏﾁｯｸSﾋﾆｮｳｷｶﾖｳｶﾝﾘｭｳ
2590003F1023	2590003F1023	612590003	<NA>     	\N	セルニルトン錠	ｾﾙﾆﾙﾄﾝｼﾞｮｳ
261270BS1023	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
2619714X1059	<NA>        	620001555	<NA>     	\N	<NA>	<NA>
2646709Q1034	2646709Q1034	620007759	<NA>     	\N	フルコート外用液０．０１％	ﾌﾙｺｰﾄｶﾞｲﾖｳｴｷ0.01%
2646729M1048	2646729M1048	620000364	<NA>     	\N	エクラー軟膏０．３％	ｴｸﾗｰﾅﾝｺｳ0.3%
264970AM1020	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
2655707N1038	2655707N1038	620007607	<NA>     	\N	エクセルダームクリーム１％	ｴｸｾﾙﾀﾞｰﾑｸﾘｰﾑ1%
2669700X1040	2669700X1016	620008410	662660001	\N	イオウ・カンフルローション「東豊」	ｲｵｳｶﾝﾌﾙﾛｰｼｮﾝﾄｳﾎｳ
2710803U1044	2710803U1044	628302301	<NA>     	\N	オーラ注歯科用カートリッジ１．８ｍＬ	ｵｰﾗﾁｭｳｼｶﾖｳｶｰﾄﾘｯｼﾞ1.8
279080BF1041	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
3122001F1027	3122001F1027	613120013	<NA>     	\N	ジセタミン錠２５　２５ｍｇ	ｼﾞｾﾀﾐﾝｼﾞｮｳ25
3135400A1026	3135400A1018	643130341	643130211	\N	フォリアミン注射液　１５ｍｇ	ﾌｫﾘｱﾐﾝﾁｭｳｼｬｴｷ
3179110M1034	3179110M1034	620720201	<NA>     	\N	ビタメジン配合カプセルＢ５０	ﾋﾞﾀﾒｼﾞﾝﾊｲｺﾞｳｶﾌﾟｾﾙB50
3222400A1058	3222400A1058	620005208	<NA>     	\N	フェジン静注４０ｍｇ　２ｍＬ	ﾌｪｼﾞﾝｼﾞｮｳﾁｭｳ40MG
3239502X1020	3239502X1020	643230026	<NA>     	\N	ハイカリック液－１号　７００ｍＬ	ﾊｲｶﾘｯｸｴｷ-1ｺﾞｳ
3259119S1029	3259119S1029	622307001	<NA>     	\N	エネーボ配合経腸用液	ｴﾈｰﾎﾞﾊｲｺﾞｳｹｲﾁｮｳﾖｳｴｷ
3311402G1027	3311402G1027	643310473	<NA>     	\N	大塚生食注ＴＮ　１００ｍＬ	ｵｵﾂｶｾｲｼｮｸﾁｭｳTN
3319539A1082	3319539A1015	620008404	643310170	\N	リンゲル液「フソー」　５００ｍＬ	ﾘﾝｹﾞﾙｴｷﾌｿｰ
3327401G1030	3327401G1030	621669501	<NA>     	\N	トラネキサム酸注１ｇシリンジ「ＮＰ」　１０％１０ｍＬ	ﾄﾗﾈｷｻﾑｻﾝﾁｭｳ1GｼﾘﾝｼﾞNP
3339950Q1074	3339950Q1074	620009049	<NA>     	\N	ヒルドイドローション０．３％	ﾋﾙﾄﾞｲﾄﾞﾛｰｼｮﾝ0.3%
3410521A1031	3410521A1031	620007213	<NA>     	\N	ＡＫ－ソリタ透析剤・ＤＰ　９Ｌ（炭酸水素ナトリウム付）	AK-ｿﾘﾀﾄｳｾｷｻﾞｲDP
3420432A1038	3420432A1038	620009138	<NA>     	\N	エクストラニール腹膜透析液　１．５Ｌ	ｴｸｽﾄﾗﾆｰﾙﾌｸﾏｸﾄｳｾｷｴｷ
3929009M1027	3929009M1027	622366701	<NA>     	\N	ニシスタゴンカプセル５０ｍｇ	ﾆｼｽﾀｺﾞﾝｶﾌﾟｾﾙ50MG
3959412F1021	3959412F1021	620004884	<NA>     	\N	マイオザイム点滴静注用５０ｍｇ	ﾏｲｵｻﾞｲﾑﾃﾝﾃｷｼﾞｮｳﾁｭｳﾖｳ
39690A7F1028	<NA>        	<NA>     	<NA>     	\N	<NA>	<NA>
3999411A1033	3999411A1033	620000212	<NA>     	\N	オザペン注２０ｍｇ　２ｍＬ	ｵｻﾞﾍﾟﾝﾁｭｳ20MG
4223701M1047	4223701M1047	620006502	<NA>     	\N	５－ＦＵ軟膏５％協和	5-FUﾅﾝｺｳ5%ｷｮｳﾜ
4299100F1026	4299100F1026	622336001	<NA>     	\N	ロンサーフ配合錠Ｔ１５　１５ｍｇ（トリフルリジン相当量）	ﾛﾝｻｰﾌﾊｲｺﾞｳｼﾞｮｳT15
4300438A1023	4300438A1023	644300006	<NA>     	\N	カルディオダイン注	ｶﾙﾃﾞｨｵﾀﾞｲﾝﾁｭｳ
4419005B1045	4419005B1045	620000138	<NA>     	\N	ペリアクチン散１％	ﾍﾟﾘｱｸﾁﾝｻﾝ1%
4490017F1029	4490017F1010	620005534	622744500	\N	プランルカスト錠１１２．５「ＥＫ」　１１２．５ｍｇ	ﾌﾟﾗﾝﾙｶｽﾄｼﾞｮｳ112.5EK
4490403A1030	4490403A1030	628035601	<NA>     	\N	治療標準アレルゲンエキストリイスギ花粉２００ＪＡＵ／ｍＬ　２ｍＬ	ﾁﾘｮｳﾖｳﾋｮｳｼﾞｭﾝｶｱﾚﾙｹﾞﾝ
5200014C1025	5200014C1025	615101282	<NA>     	\N	三和葛根加朮附湯エキス細粒	ｻﾝﾜｶｯｺﾝｶｼﾞｭﾂﾌﾞﾄｳｴｷｽｻ
5200051D1021	5200051D1021	615101033	<NA>     	\N	オースギ柴胡桂枝湯エキスＧ	ｵｰｽｷﾞｻｲｺｹｲｼﾄｳｴｷｽG
5200070D1024	5200070D1024	615101663	<NA>     	\N	オースギ十味敗毒湯エキスＧ	ｵｰｽｷﾞｼﾞｭｳﾐﾊｲﾄﾞｸﾄｳｴｷｽ
5200091D1026	5200091D1026	615101468	<NA>     	\N	ツムラ大黄牡丹皮湯エキス顆粒（医療用）	ﾂﾑﾗﾀﾞｲｵｳﾎﾞﾀﾝﾋﾟﾄｳｴｷｽｶ
5200113D1024	5200113D1024	615101486	<NA>     	\N	ツムラ二朮湯エキス顆粒（医療用）	ﾂﾑﾗﾆｼﾞｭﾂﾄｳｴｷｽｶﾘｭｳｲﾘｮ
5200129D1025	5200129D1025	615101056	<NA>     	\N	オースギ防已黄耆湯エキスＧ	ｵｰｽｷﾞﾎﾞｳｲｵｳｷﾞﾄｳｴｷｽG
5200144C1024	5200144C1024	615101262	<NA>     	\N	コタロー苓姜朮甘湯エキス細粒	ｺﾀﾛｰﾘｮｳｷｮｳｼﾞｭﾂｶﾝﾄｳｴｷ
6123401A1031	6123401A1031	640453139	<NA>     	\N	トブラシン注小児用１０ｍｇ	ﾄﾌﾞﾗｼﾝﾁｭｳｼｮｳﾆﾖｳ10MG
6132009F1023	6132009F1023	616130407	<NA>     	\N	トミロン錠５０　５０ｍｇ	ﾄﾐﾛﾝｼﾞｮｳ50
6199104X1023	6199104X1023	622485401	<NA>     	\N	ボノサップパック４００	ﾎﾞﾉｻｯﾌﾟﾊﾟｯｸ400
6241019F1021	6241019F1021	622696001	<NA>     	\N	ラスビック錠７５ｍｇ	ﾗｽﾋﾞｯｸｼﾞｮｳ75MG
6290002M1020	6290002M1020	616290164	<NA>     	\N	ジフルカンカプセル５０ｍｇ	ｼﾞﾌﾙｶﾝｶﾌﾟｾﾙ50MG
6343400X1041	6343400X1041	621151301	<NA>     	\N	献血ベニロン－Ｉ静注用５００ｍｇ　１０ｍＬ（溶解液付）	ｹﾝｹﾂﾍﾞﾆﾛﾝ-Iｼﾞｮｳﾁｭｳﾖｳ
6343442D1022	6343442D1022	622402801	<NA>     	\N	イロクテイト静注用２５０　２５０国際単位（溶解液付）	ｲﾛｸﾃｲﾄｼﾞｮｳﾁｭｳﾖｳ250
6399500D2037	6399500D2037	621513701	<NA>     	\N	ヒスタグロビン注　人免疫グロブリン１２ヒスタミン二塩酸塩０．１５	ﾋｽﾀｸﾞﾛﾋﾞﾝﾋｶﾁｭｳﾖｳ
7121709X1094	7121709X1094	621170215	<NA>     	\N	単軟膏（東豊）	ﾀﾝﾅﾝｺｳﾄｳﾎｳ
7212010X1029	7212010X1029	617210103	<NA>     	\N	バリトゲン－デラックス　９７．９８％	ﾊﾞﾘﾄｹﾞﾝ-ﾃﾞﾗｯｸｽ
7219415G1041	7219415G1041	620007193	<NA>     	\N	イオパーク２４０注シリンジ１００ｍＬ　５１．７７％	ｲｵﾊﾟｰｸ240ﾁｭｳｼﾘﾝｼﾞ100
7290004B1030	7290004B1030	620004563	<NA>     	\N	フェリセルツ散２０％　６００ｍｇ	ﾌｪﾘｾﾙﾂｻﾝ20%
7319700Q1032	7319700Q1032	667310009	<NA>     	\N	アンソッコウチンキ（小堺）	ｱﾝｿｯｺｳﾁﾝｷｺｻﾞｶｲ
8219502A1028	8219502A1028	648210007	<NA>     	\N	弱ペチロルファン注射液　１ｍＬ	ｼﾞｬｸﾍﾟﾁﾛﾙﾌｧﾝﾁｭｳｼｬｴｷ
\.


--
-- TOC entry 3056 (class 0 OID 16398)
-- Dependencies: 198
-- Data for Name: yj_transition; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.yj_transition (old_yj_code, current_yj_code) FROM stdin;
\.


--
-- TOC entry 3099 (class 0 OID 0)
-- Dependencies: 214
-- Name: ae_extract_dict_ae_extract_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.ae_extract_dict_ae_extract_id_seq', 1, false);


--
-- TOC entry 3100 (class 0 OID 0)
-- Dependencies: 215
-- Name: ae_std_dict_ae_std_dict_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.ae_std_dict_ae_std_dict_id_seq', 1, false);


--
-- TOC entry 3101 (class 0 OID 0)
-- Dependencies: 216
-- Name: allergy_master_allergy_master_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.allergy_master_allergy_master_id_seq', 1, false);


--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 218
-- Name: contraindication_contraindication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.contraindication_contraindication_id_seq', 1, false);


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 220
-- Name: day_limitation_day_limitation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.day_limitation_day_limitation_id_seq', 1, false);


--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 221
-- Name: dose_dose_id; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.dose_dose_id', 1, false);


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 222
-- Name: drug_basic_info_drug_basic_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.drug_basic_info_drug_basic_info_id_seq', 109, true);


--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 224
-- Name: drug_identification_drug_identification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.drug_identification_drug_identification_id_seq', 1, false);


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 226
-- Name: drug_price_drug_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.drug_price_drug_price_id_seq', 99, true);


--
-- TOC entry 3108 (class 0 OID 0)
-- Dependencies: 227
-- Name: early_symptom_early_symptom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.early_symptom_early_symptom_id_seq', 1, false);


--
-- TOC entry 3109 (class 0 OID 0)
-- Dependencies: 228
-- Name: efficacy_class_master_efficacy_class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.efficacy_class_master_efficacy_class_id_seq', 1, false);


--
-- TOC entry 3110 (class 0 OID 0)
-- Dependencies: 229
-- Name: efficacy_class_std_dict_efficacy_class_std_dict_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.efficacy_class_std_dict_efficacy_class_std_dict_id_seq', 1, false);


--
-- TOC entry 3111 (class 0 OID 0)
-- Dependencies: 230
-- Name: guidance_master_guidance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.guidance_master_guidance_id_seq', 1, false);


--
-- TOC entry 3112 (class 0 OID 0)
-- Dependencies: 231
-- Name: high_risk_guidance_master_high_risk_guidance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.high_risk_guidance_master_high_risk_guidance_id_seq', 1, false);


--
-- TOC entry 3113 (class 0 OID 0)
-- Dependencies: 232
-- Name: indication_dose_text_indication_dose_text_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.indication_dose_text_indication_dose_text_id_seq', 1, false);


--
-- TOC entry 3114 (class 0 OID 0)
-- Dependencies: 234
-- Name: interaction_interaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.interaction_interaction_id_seq', 1, false);


-- Completed on 2021-01-04 11:12:13 UTC

--
-- PostgreSQL database dump complete
--

